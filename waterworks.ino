#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ESP8266TrueRandom.h>
#include <SPI.h>
#include <EEPROM.h>

const char* device_name = "waterworks";   // This device is waterworks.
int upload_interval = 1000;       // Uploading water depth and valve state in miliseconds.

/*    --------  Constant values -----------        */

const char* ssid = "WiFi560-AP";              // RPi3's AP name
const char* password = "Shane_wifi560";       // RPi3's AP password
const char* mqtt_server = "172.24.1.1";   // Server has been built on the router(RPi 3) itself

/*    --------  topic setting ----------------      */

const char* valve_set_suffix = "/valve_set";
const char* watertank_depth_set_suffix = "/watertank_depth/set";

const char* valve_state_suffix = "/valve_state";
const char* watertank_depth_suffix = "/watertank_depth";
const char* watertank_depth_alarm_suffix = "/watertank_depth/alarm";
const char* watertank_depth_setting_suffix = "/watertank_depth/setting";

// Pin setting

int PIN_MS5540_mclk = 5;  // MCLK pin for MS5540 sensor (Use GPIO5)
int PIN_valveOn = 16;  // pin to open valve  (Use GPIO16)
int PIN_valveOn_Led = 4;  // pin to notice valve on (Use GPIO4)
int PIN_overalarm_Led = 0;  // pin to notice overalarm  (Use GPIO0)
int PIN_shortalarm_Led = 2;  // pin to notice shortalarm (Use GPIO2)

// Global variables

WiFiClient espClient;
PubSubClient client(espClient);

byte uuidNumber[16]; // UUIDs in binary form are 16 bytes long
String uuid_buf;     // UUID in string byte
char uuid_name[37];  // UUID in char array type

long lastMsg = 0;
int ATM_pressure = 998;   //ATM_pressure is atmospheric pressure. This value must be revised before the using the device

float pressure, curr_depth;
char* depth_set;  //  low depth from mobile app(char* type)
float val_depth_set;  // low depth from mobile app(float type)

float watertank_depth_low = 20;  // permissible minimum watertank depth in cm unit
//float watertank_depth_top = 100;

const char* valve_state = "OFF";
const char* watertank_state = "NORMAL";

char* buf_curr_depth = new char[10];
char* buf_setting_depth = new char[10];

char buf_pub_topic[50];
char buf_sub_topic[50];

void setup() {

  Serial.begin(115200);

  EEPROM.begin(512);  // store values of valve set and depth  by mobile app

  pinMode(PIN_valveOn, OUTPUT);
  digitalWrite(PIN_valveOn, LOW);
  pinMode(PIN_valveOn_Led, OUTPUT);
  digitalWrite(PIN_valveOn_Led, LOW);
  pinMode(PIN_overalarm_Led, OUTPUT);
  digitalWrite(PIN_overalarm_Led, LOW);
  pinMode(PIN_shortalarm_Led, OUTPUT);
  digitalWrite(PIN_shortalarm_Led, LOW);

  /*  analogWriteFreq(10);
    analogWriteRange(256);
    analogWrite(PIN_MS5540_mclk, 128);
  */
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  delay(2000);

  setup_sensor();
  delay(500);

  ESP8266TrueRandom.uuid(uuidNumber);
  Serial.print("The UUID number is ");
  printUuid(uuidNumber);
  Serial.println();
  uuid_buf = ESP8266TrueRandom.uuidToString(uuidNumber);
  uuid_buf.toCharArray(uuid_name, 37);
  Serial.println(uuid_name);
}

void setup_wifi() {

  int attempt = 0;
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {

    // Try to connect for 15 sec, and restart nodemcu
    if (attempt < 30)
      attempt ++;
    else
      ESP.restart();

    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup_sensor() {
  SPI.begin();

  SPI.setBitOrder(MSBFIRST);
  SPI.setFrequency(500000);
  //SPI.setClockDivider(SPI_CLOCK_DIV32);   // divide 16MHz to communicate on 500KHZ

  pinMode(PIN_MS5540_mclk, OUTPUT);
  delay(100);
}

void resetsensor() {
  SPI.setDataMode(SPI_MODE0);
  SPI.transfer(0x15);
  SPI.transfer(0x55);
  SPI.transfer(0x40);
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println("");

  // Check topic of valve set and open led
  set_sub_topic(valve_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

    if (!strncmp((const char*)payload, "ON", 2)) {

      valve_state = "ON";
      Serial.println("Current state of valve is ON...");
      digitalWrite(PIN_valveOn_Led, HIGH);
      digitalWrite(PIN_valveOn, HIGH);
    }
    else if (!strncmp((const char*)payload, "OFF", 3)) {
      valve_state = "OFF";
      Serial.println("Current state of valve is OFF...");
      digitalWrite(PIN_valveOn_Led, LOW);
      digitalWrite(PIN_valveOn, LOW);
    }
  }
  // Check topic of watertank depth, and than control valve by it.
  else {
    set_sub_topic(watertank_depth_set_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

      depth_set = (char*)payload;
      val_depth_set = atoi(depth_set);
      
      Serial.print("Watertank depth was set:  ");
      Serial.println(val_depth_set);
    }
  }
  EEPROM_Write();
}

void printHex(byte number) {
  int topDigit = number >> 4;
  int bottomDigit = number & 0x0f;
  // Print high hex digit
  Serial.print( "0123456789ABCDEF"[topDigit] );
  // Low hex digit
  Serial.print( "0123456789ABCDEF"[bottomDigit] );
}

void printUuid(byte* uuidNumber) {
  int i;
  for (i = 0; i < 16; i++) {
    if (i == 4) Serial.print("-");
    if (i == 6) Serial.print("-");
    if (i == 8) Serial.print("-");
    if (i == 10) Serial.print("-");
    printHex(uuidNumber[i]);
  }
}

void reconnect() {
  // Loop until we're reconnected

  int attempt = 0;

  while (!client.connected()) {

    if (attempt < 3)
      attempt ++;
    else
      ESP.restart();

    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("uuid_name")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe
      set_sub_topic(valve_set_suffix);
      client.subscribe(buf_sub_topic);
      set_sub_topic(watertank_depth_set_suffix);
      client.subscribe(buf_sub_topic);
      client.subscribe("common");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void EEPROM_Write(void) {

  byte val_state;
  if (valve_state == "ON") {
    val_state = 1;
  }
  else if (valve_state == "OFF") {
    val_state = 0;
  }

  EEPROM.write(0, val_depth_set);
  EEPROM.write(1, val_state);
  EEPROM.commit();
}

void EEPROM_Read(void) {

  byte val_state;

  val_state = EEPROM.read(1);
  if (val_state == 1) {
    valve_state = "ON";
    digitalWrite(PIN_valveOn_Led, HIGH);
    digitalWrite(PIN_valveOn, HIGH);
  }
  else if (val_state == 0) {
    valve_state = "OFF";
    digitalWrite(PIN_valveOn_Led, LOW);
    digitalWrite(PIN_valveOn, LOW);
  }

  val_depth_set = EEPROM.read(0);
}

void loop() {
  if (!client.connected()) {
    reconnect();
    EEPROM_Read();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > upload_interval) {
    lastMsg = now;

    pressure = read_MS5540();
    curr_depth = pressure - ATM_pressure;   //
    Serial.print("Current depth of watertank is ");
    Serial.print(curr_depth);
    Serial.println("cm");

    //char* buf_curr_depth = new char[10];
    dtostrf(curr_depth, 5, 1, buf_curr_depth);

    dtostrf(val_depth_set, 5, 1, buf_setting_depth);
    
    if (curr_depth > val_depth_set) {
      digitalWrite(PIN_valveOn, LOW);
      digitalWrite(PIN_valveOn_Led, LOW);
      digitalWrite(PIN_overalarm_Led, HIGH);
      digitalWrite(PIN_shortalarm_Led, LOW);
      valve_state = "OFF";
      watertank_state = "OVER";
      Serial.println("Now the depth of watertank exceeded setting depth.");
    }
    if (curr_depth < watertank_depth_low) {
      digitalWrite(PIN_shortalarm_Led, HIGH);
      digitalWrite(PIN_overalarm_Led, LOW);
      //digitalWrite(PIN_valveOn_Led, HIGH);
      //digitalWrite(PIN_valveOn, HIGH);
      watertank_state = "SHORT";
      //valve_state = "ON";
      Serial.println("Now the depth of watertank is lower than permissible minimum depth.");
    }
    if ((watertank_depth_low < curr_depth) && (curr_depth < val_depth_set)) {
      digitalWrite(PIN_overalarm_Led, LOW);
      digitalWrite(PIN_shortalarm_Led, LOW);
      watertank_state = "NORMAL";
    }
    // publish valve state
    set_pub_topic(valve_state_suffix);
    client.publish(buf_pub_topic, valve_state);
    // publish watertank state
    set_pub_topic(watertank_depth_alarm_suffix);
    client.publish(buf_pub_topic, watertank_state);
    // publish cuurent depth of watertank
    set_pub_topic(watertank_depth_suffix);
    client.publish(buf_pub_topic, buf_curr_depth);
    
    set_pub_topic(watertank_depth_setting_suffix);
    client.publish(buf_pub_topic, buf_setting_depth);
  }
  else {
    delay(500);  // Loop function takes about 300ms, so 400 ms is enough.
  }
}
int read_MS5540() {

  //TCCR1B = (TCCR1B & 0xF8) | 1 ; //generates the MCKL signal
  //analogWrite(PIN_MS5540_mclk, 128);
  //Serial.println("HAHAHA d7kt 3leek");

  /*#ifdef __AVR__
    TCCR1B = (TCCR1B & 0xF8) | 1 ; //generates the MCKL signal
    Serial.println("HAHAHA d7kt 3leek");
    #else
    analogWriteFreq(10);
    analogWriteRange(256);
    #endif
    analogWrite(PIN_MS5540_mclk, 128);
  */

  analogWriteFreq(35000);  // MCLK = 35Khz
  analogWriteRange(256);
  analogWrite(PIN_MS5540_mclk, 128);

  resetsensor(); //resets the sensor - caution: afterwards mode = SPI_MODE0!

  //Calibration word 1
  unsigned int word1 = 0;
  unsigned int word11 = 0;
  SPI.transfer(0x1D); //send first byte of command to get calibration word 1
  SPI.transfer(0x50); //send second byte of command to get calibration word 1
  SPI.setDataMode(SPI_MODE1); //change mode in order to listen
  word1 = SPI.transfer(0x00); //send dummy byte to read first byte of word
  word1 = word1 << 8; //shift returned byte
  word11 = SPI.transfer(0x00); //send dummy byte to read second byte of word
  word1 = word1 | word11; //combine first and second byte of word

  resetsensor();//resets the sensor

  //Calibration word 2; see comments on calibration word 1
  unsigned int word2 = 0;
  byte word22 = 0;
  SPI.transfer(0x1D);
  SPI.transfer(0x60);
  SPI.setDataMode(SPI_MODE1);
  word2 = SPI.transfer(0x00);
  word2 = word2 << 8;
  word22 = SPI.transfer(0x00);
  word2 = word2 | word22;

  resetsensor();//resets the sensor

  //Calibration word 3; see comments on calibration word 1
  unsigned int word3 = 0;
  byte word33 = 0;
  SPI.transfer(0x1D);
  SPI.transfer(0x90);
  SPI.setDataMode(SPI_MODE1);
  word3 = SPI.transfer(0x00);
  word3 = word3 << 8;
  word33 = SPI.transfer(0x00);
  word3 = word3 | word33;

  resetsensor();//resets the sensor

  //Calibration word 4; see comments on calibration word 1
  unsigned int word4 = 0;
  byte word44 = 0;
  SPI.transfer(0x1D);
  SPI.transfer(0xA0);
  SPI.setDataMode(SPI_MODE1);
  word4 = SPI.transfer(0x00);
  word4 = word4 << 8;
  word44 = SPI.transfer(0x00);
  word4 = word4 | word44;

  long c1 = word1 >> 1;
  long c2 = ((word3 & 0x3F) << 6) | ((word4 & 0x3F));
  long c3 = (word4 >> 6) ;
  long c4 = (word3 >> 6);
  long c5 = (word2 >> 6) | ((word1 & 0x1) << 10);
  long c6 = word2 & 0x3F;

  resetsensor();//resets the sensor

  //Temperature:
  unsigned int tempMSB = 0; //first byte of value
  unsigned int tempLSB = 0; //last byte of value
  unsigned int D2 = 0;
  SPI.transfer(0x0F); //send first byte of command to get temperature value
  SPI.transfer(0x20); //send second byte of command to get temperature value
  delay(35); //wait for conversion end
  SPI.setDataMode(SPI_MODE1); //change mode in order to listen
  tempMSB = SPI.transfer(0x00); //send dummy byte to read first byte of value
  tempMSB = tempMSB << 8; //shift first byte
  tempLSB = SPI.transfer(0x00); //send dummy byte to read second byte of value
  D2 = tempMSB | tempLSB; //combine first and second byte of value

  resetsensor();//resets the sensor

  //Pressure:
  unsigned int presMSB = 0; //first byte of value
  unsigned int presLSB = 0; //last byte of value
  unsigned int D1 = 0;
  SPI.transfer(0x0F); //send first byte of command to get pressure value
  SPI.transfer(0x40); //send second byte of command to get pressure value
  delay(35); //wait for conversion end
  SPI.setDataMode(SPI_MODE1); //change mode in order to listen
  presMSB = SPI.transfer(0x00); //send dummy byte to read first byte of value
  presMSB = presMSB << 8; //shift first byte
  presLSB = SPI.transfer(0x00); //send dummy byte to read second byte of value
  D1 = presMSB | presLSB;

  const long UT1 = (c5 * 8) + 20224;
  const long dT = (D2 - UT1);
  const long TEMP = 200 + ((dT * (c6 + 50)) / 1024);
  const long OFF  = (c2 * 4) + (((c4 - 512) * dT) / 4096);
  const long SENS = c1 + ((c3 * dT) / 1024) + 24576;

  long PCOMP = ((((SENS * (D1 - 7168)) / 16384) - OFF) / 32) + 250;
  float TEMPREAL = TEMP / 10;
  Serial.print("pressure =    ");
  Serial.print(PCOMP);
  Serial.println(" mbar");

  const long dT2 = dT - ((dT >> 7 * dT >> 7) >> 3);
  const float TEMPCOMP = (200 + (dT2 * (c6 + 100) >> 11)) / 10;
  Serial.print("temperature = ");
  Serial.print(TEMPCOMP);
  Serial.println(" °C");
  Serial.println("************************************");
  return PCOMP; // in mbar
  delay(1000);
}

void set_pub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}

